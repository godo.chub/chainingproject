﻿using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using System.Linq;

public class ChainingAndContinuation
{
    public static void Main()
    {
        Task<int[]> operationOne = Task.Run(() =>
        {
            return GenerateArray();
        });

        Console.WriteLine(string.Join(",", operationOne.Result));

        Task<int[]> operationTwo = operationOne.ContinueWith((x) =>
        {
            return MultipleArray(operationOne.Result);
        });

        Console.WriteLine(string.Join(",", operationTwo.Result));

        Task<int[]> operationThree = operationTwo.ContinueWith((x) =>
        {
            return SortedArray(operationTwo.Result);
        });

        Console.WriteLine(string.Join(",", operationThree.Result));

        Task<double> operationFour = operationThree.ContinueWith((x) =>
        {
            return Average(operationThree.Result);
        });

        Console.WriteLine(operationFour.Result);
    }

    public static int[] GenerateArray()
    {
        int Min = 0;
        int Max = 50;

        int[] arr = new int[5];

        Random randNum = new();
        for (int i = 0; i < arr.Length; i++)
        {
            arr[i] = randNum.Next(Min, Max);
        }

        return arr;
    }

    public static int[] MultipleArray(int[] arr)
    {
        int Min = 0;
        int Max = 50;
        Random random = new();
        int multiplier = random.Next(Min, Max);

        for (int i = 0; i < arr.Length; ++i)
            arr[i] *= multiplier;

        return arr;
    }

    public static int[] SortedArray(int[] arr)
    {
        return arr.OrderBy(x => x).ToArray();
    }

    public static double Average(int[] arr)
    {
        return Queryable.Average(arr.AsQueryable());
    }
}